require('dotenv').config();
const express = require('express');
const app = express();

const mongoose = require('mongoose');
const morgan = require('morgan');
const cookieParser = require('cookie-parser');

const {asyncErrorHandle} = require('./server/utils/api.util');
const {NotFoundError} = require('./server/utils/errors/NotFoundError');
const {userRouter} = require('./server/controllers/user.router');
const {notesRouter} = require('./server/controllers/notes.router');
const {authRouter} = require('./server/controllers/auth.router');
const {authMiddleware} = require('./server/middlewares/auth.middleware.js');


app.use(morgan('tiny'));
app.use(express.json());
app.use(cookieParser());

app.use(express.static('dist'));

app.use('/api/auth', authRouter);

app.use('/api/users', [authMiddleware], userRouter);
app.use('/api/notes', [authMiddleware], notesRouter);

app.use(asyncErrorHandle(async () => {
  throw new NotFoundError();
}));

app.use((err, req, res, next) => {
  const resStatus = err.status || 500;
  const resMessage = err.message || 'Server error';
  res.status(resStatus).json({message: resMessage});
});


(async () => {
  try {
    connectToBD();
    console.log('DB connected');
    app.listen(process.env.HOST_PORT);
  } catch (error) {
    console.log(`Error: Error while server startup: ${error.message}`);
  }
})();

/** Connect to MongoDB */
async function connectToBD() {
  await mongoose.connect(process.env.DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });
}

// TODO frontend visualization
