FROM node:14-alpine
ENV NODE_ENV=production
WORKDIR /usr/src/app
COPY ["package*.json", "./"]
RUN npm install
RUN npm install -g @angular/cli@12.2.1
COPY . .
EXPOSE 8080
CMD ["npm", "start"]
