const Joi = require('joi');

const usernameSchema = Joi.string().min(2);
const passwordSchema = Joi.string().min(4);
const noteContentSchema = Joi.string();

const loginObjSchema = Joi.object({
  username: usernameSchema.required(),
  password: passwordSchema.required(),
});

const passwordChangedSchema = Joi.object({
  oldPassword: passwordSchema.required(),
  newPassword: passwordSchema.required(),
});

const noteContentObjSchema = Joi.object({
  text: noteContentSchema.required(),
});

module.exports = {
  loginObjSchema,
  registerObjSchema: loginObjSchema,
  passwordChangedSchema,
  noteContentObjSchema,
};
