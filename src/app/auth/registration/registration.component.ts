import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {Subscription} from "rxjs";
import {JoiValidationService} from "../../shared/joi-validation.service";
import {AuthService} from "../../shared/auth.service";
import {Router} from "@angular/router";

const {registerObjSchema} = require('../../../../shared/validation.schema.js');

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit, OnDestroy {
  formGroup: FormGroup
  subscription: Subscription
  resErrorMessage: string

  constructor(
    private joiValidationService: JoiValidationService,
    private authService: AuthService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    const registrationGroupValidator =
      this.joiValidationService.createValidator(registerObjSchema)

    this.formGroup = new FormGroup({
      username: new FormControl(),
      password: new FormControl(),
    }, {
      validators: registrationGroupValidator,
    })
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe()
  }

  async onSubmit() {
    this.formGroup.disable()
    this.subscription = this.authService.register(this.formGroup.value)
      .subscribe({
        next: (res) => {
          this.router.navigate(['/login'])
        },
        error: (res) => {
          this.resErrorMessage = res.error.message
          this.formGroup.enable()
        }
      });
  }

  getError(fieldName: string): string | null {
    if (this.formGroup.get(fieldName)?.touched && this.formGroup.errors) {
      return this.formGroup.errors[fieldName]
    }
    return null
  }
}
