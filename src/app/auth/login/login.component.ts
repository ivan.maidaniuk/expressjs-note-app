import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";

import {JoiValidationService} from "../../shared/joi-validation.service";
import {AuthService} from "../../shared/auth.service";
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";

const {loginObjSchema} = require('../../../../shared/validation.schema.js');

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  formGroup: FormGroup
  subscription: Subscription
  resErrorMessage: string
  previouslyAccessDenied = false

  constructor(
    private joiValidationService: JoiValidationService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    const loginGroupValidator =
      this.joiValidationService.createValidator(loginObjSchema)

    this.formGroup = new FormGroup({
      username: new FormControl(),
      password: new FormControl(),
    }, {
      validators: loginGroupValidator,
    })

    this.route.queryParams.subscribe((params) => {
      if (params['access-denied']) {
        this.previouslyAccessDenied = true
      }
    })
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe()
  }

  async onSubmit() {
    this.formGroup.disable()
    this.subscription = this.authService.login(this.formGroup.value)
      .subscribe({
        next: (res) => {
          this.router.navigate(['/'])
        },
        error: (res) => {
          this.resErrorMessage = res.error.message
          this.formGroup.enable()
        }
      });
  }

  getError(fieldName: string): string | null {
    if (this.formGroup.get(fieldName)?.touched && this.formGroup.errors) {
      return this.formGroup.errors[fieldName]
    }
    return null
  }
}
