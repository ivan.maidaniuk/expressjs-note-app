import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RegistrationComponent} from "./auth/registration/registration.component";
import {LoginComponent} from "./auth/login/login.component";
import {PageNotFoundComponent} from "./page-not-found/page-not-found.component";
import {AuthGuard} from "./shared/auth.guard";
import {UserProfileComponent} from "./user-profile/user-profile.component";
import {NotesArchiveComponent} from "./notes/notes-archive/notes-archive.component";
import {NoteSingeComponent} from "./notes/note-singe/note-singe.component";
import {NoteSingeResolver} from "./notes/note-singe/note-singe.resolver";
import {NotesArchiveResolver} from "./notes/notes-archive/notes-archive.resolver";
import {UserProfileResolver} from "./user-profile/user-profile.resolver";

const routes: Routes = [
  {path: 'registration', component: RegistrationComponent},
  {path: 'login', component: LoginComponent},

  {
    path: '', canActivate: [AuthGuard], children: [
      {path: '', redirectTo: 'notes', pathMatch: 'full'},
      {path: 'notes', component: NotesArchiveComponent, resolve: {notes: NotesArchiveResolver}},
      {path: 'notes/:id', component: NoteSingeComponent, resolve: {note: NoteSingeResolver}},
      {path: 'profile', component: UserProfileComponent, resolve: {user: UserProfileResolver}}
    ]
  },

  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: '404', component: PageNotFoundComponent},
  {path: '**', redirectTo: '404'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
