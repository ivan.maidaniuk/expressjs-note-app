export interface UserProfile {
  _id: string,
  username: string,
  createdAt: Date,

  [key: string]: any
}
