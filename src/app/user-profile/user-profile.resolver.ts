import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import {UserProfileService} from "./user-profile.service";

@Injectable({
  providedIn: 'root'
})
export class UserProfileResolver {
  constructor(private userService: UserProfileService) {
  }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.userService.getUserData();
  }
}
