import {Component, OnInit} from '@angular/core';
import {UserProfile} from "./user-profile";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  user: UserProfile

  constructor(
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    const {user} = this.route.snapshot.data.user
    this.user = user
  }
}
