import {Component, Input, OnChanges, OnInit, Output, EventEmitter, SimpleChanges} from '@angular/core';
import {Note} from "../note";
import {NotesService} from "../notes.service";
import {FormControl, Validators} from "@angular/forms";

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent implements OnInit, OnChanges {
  @Input() note: Note
  @Input() shouldHideDetailsBtn?: boolean = false
  @Output() noteDeleteEmitter = new EventEmitter<string>()
  checkbox: FormControl
  textControl: FormControl
  initialText: string

  constructor(private notesService: NotesService) {
  }

  ngOnInit(): void {

  }

  ngOnChanges(changes: SimpleChanges) {
    this.checkbox = new FormControl(this.note?.completed)
    this.textControl = new FormControl(this.note?.text, Validators.required)
    this.initialText = this.note?.text
  }

  toggleCompletion() {
    this.notesService.toggleCompletion(this.note._id).subscribe()
  }

  saveText($event: Event) {
    $event.preventDefault()
    if (this.textControl.valid) {
      this.initialText = this.textControl.value
      this.notesService.saveText(this.note._id, this.textControl.value).subscribe()
    }
  }

  resetText() {
    this.textControl.setValue(this.initialText)
  }

  delete() {
    this.notesService.deleteNoteById(this.note._id).subscribe(() => {
      this.noteDeleteEmitter.emit(this.note._id)
    })
  }
}
