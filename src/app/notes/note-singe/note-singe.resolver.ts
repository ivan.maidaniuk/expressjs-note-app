import {Injectable} from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import {NotesService} from "../notes.service";


@Injectable({
  providedIn: 'root'
})
export class NoteSingeResolver {
  constructor(private notesService: NotesService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.notesService.getNoteById(route.params.id);
  }
}
