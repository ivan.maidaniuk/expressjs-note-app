import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Note} from "../note";
import {NotesService} from "../notes.service";

@Component({
  selector: 'app-note-singe',
  templateUrl: './note-singe.component.html',
  styleUrls: ['./note-singe.component.scss']
})
export class NoteSingeComponent implements OnInit {
  note: Note

  constructor(
    private noteService: NotesService,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.note = this.route.snapshot.data['note']
  }

  goToArchiveWithExcludingNote(noteId: string) {
    this.router.navigate(
      ['/notes'],
      {
        queryParams: {
          'exclude-note-id': noteId,
        }
      }
    )
  }
}
