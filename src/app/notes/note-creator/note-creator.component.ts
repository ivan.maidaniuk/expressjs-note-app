import {Component, OnInit, Output, EventEmitter, ViewChild, ElementRef} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {NotesService} from "../notes.service";

const {noteContentObjSchema} = require('../../../../shared/validation.schema.js');

@Component({
  selector: 'app-note-creator',
  templateUrl: './note-creator.component.html',
  styleUrls: ['./note-creator.component.scss']
})
export class NoteCreatorComponent implements OnInit {
  @Output() updateArchive = new EventEmitter()
  textContent: FormControl
  @ViewChild('textField') textField: ElementRef

  constructor(private notesService: NotesService) {
  }

  ngOnInit(): void {
    this.textContent = new FormControl('', Validators.required)
  }

  createNote($event: Event) {
    if (this.textContent.valid) {
      $event.preventDefault()
      this.textContent.disable()
      this.notesService.createNote(this.textContent.value).subscribe(() => {
        this.purgeTextContent()
        this.textContent.enable()
        this.updateArchive.emit()
      })
    }
  }

  private purgeTextContent() {
    this.textContent.setValue('')
    this.textContent.markAsPristine()
    this.textField.nativeElement.focus()
  }
}
