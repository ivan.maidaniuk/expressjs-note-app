export interface Note {
  completed: boolean
  createdDate: Date
  text: string
  userId: string
  _id: string

  [key: string]: any
}

export interface NotesResponse {
  notes: Note[]
  offset: number
  limit: number
  count: number
}
