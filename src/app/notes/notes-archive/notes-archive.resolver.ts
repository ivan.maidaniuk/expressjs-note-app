import {Injectable} from '@angular/core';
import {
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import {NotesService} from "../notes.service";

@Injectable({
  providedIn: 'root'
})
export class NotesArchiveResolver {
  constructor(private notesService: NotesService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.notesService.getNotes(route.queryParams);
  }
}
