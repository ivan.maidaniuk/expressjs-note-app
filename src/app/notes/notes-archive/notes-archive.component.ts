import {Component, OnInit} from '@angular/core';
import {NotesService} from "../notes.service";
import {Note} from "../note";
import {ActivatedRoute} from "@angular/router";
import {timeout} from "rxjs/operators";

@Component({
  selector: 'app-archive',
  templateUrl: './notes-archive.component.html',
  styleUrls: ['./notes-archive.component.scss']
})
export class NotesArchiveComponent implements OnInit {
  notes: Note[]
  dataIsLoading = true

  constructor(
    private notesService: NotesService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    const {notes} = this.route.snapshot.data.notes
    this.notes = notes
    this.dataIsLoading = false
  }

  removeNoteById(noteId: string) {
    const indexOfNote = this.notes.findIndex((note) => note._id === noteId)
    this.notes.splice(indexOfNote, 1)
  }

  updateArchive() {
    this.dataIsLoading = true
    this.notesService.getNotes().subscribe(({notes}) => {
      this.notes = notes
      this.dataIsLoading = false
    })
  }
}
