import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Note, NotesResponse} from "./note";
import {filter, map, mergeMap} from "rxjs/operators";
import {ActivatedRoute, Router} from "@angular/router";
import {query} from "@angular/animations";
import * as Url from "url";

interface MessageRes {
  message: string,
}

@Injectable({
  providedIn: 'root'
})
export class NotesService {
  notes: Note[]

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute
  ) {
  }

  getNotes(queryParams: { [key: string]: string } = {}): Observable<NotesResponse> {
    return this.http.get<NotesResponse>('/api/notes', {
      params: {...queryParams}
    })
  }

  getNoteById(noteId: string): Observable<Note> {
    return this.http.get<Note>(`/api/notes/${noteId}`)
      .pipe(
        map(({note}) => note)
      )
  }

  toggleCompletion(noteId: string): Observable<MessageRes> {
    return this.http.patch<MessageRes>(`/api/notes/${noteId}`, null)
  }

  saveText(noteId: string, text: string): Observable<MessageRes> {
    return this.http.put<MessageRes>(`/api/notes/${noteId}`, {text})
  }

  deleteNoteById(noteId: string): Observable<MessageRes> {
    return this.http.delete<MessageRes>(`/api/notes/${noteId}`)
  }

  createNote(text: string): Observable<MessageRes> {
    return this.http.post<MessageRes>(`/api/notes/`, {text})
  }
}
