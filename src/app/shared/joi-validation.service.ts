import {Injectable} from "@angular/core";
import {AbstractControl, ValidationErrors, ValidatorFn} from "@angular/forms";
import * as Joi from "joi";

interface FormattedErrorObj {
  [key: string]: string
}

@Injectable({
  providedIn: 'root'
})
export class JoiValidationService {
  createValidator(scheme: Joi.AnySchema): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const {error: resultError} = scheme.validate(control.value);
      if (resultError) {
        const formattedErrorObj = JoiValidationService.getFormattedErrorObject(resultError)
        JoiValidationService.applyErrorsToControl(control, formattedErrorObj)
        return formattedErrorObj
      }
      return null
    }
  }

  private static getFormattedErrorObject(validationResult: Joi.ValidationError): FormattedErrorObj {
    return validationResult.details.reduce((acc: FormattedErrorObj, current) => {
      const key = current.path.join('.');
      acc[key] = current.message;
      return acc;
    }, {})
  }

  private static applyErrorsToControl(control: AbstractControl, formattedErrorObj: FormattedErrorObj) {
    Object.entries(formattedErrorObj).forEach(([key, value]) => {
      const iteratedControl = control.get(key);
      if (iteratedControl) {
        iteratedControl.setErrors({[key]: value});
      }
    })
  }
}
