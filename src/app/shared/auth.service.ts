import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";

interface LoginRequest {
  username: string,
  password: string,
}

interface RegisterRequest {
  username: string,
  password: string,
}

interface LoginResponse {
  message: string,
  jwt_token: string
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  _isAuthorized: boolean

  constructor(private http: HttpClient) {
  }

  login(userData: LoginRequest): Observable<LoginResponse> {
    return this.http.post<LoginResponse>('/api/auth/login', userData)
      .pipe(
        tap(data => this._isAuthorized = true)
      )
  }

  register(userData: RegisterRequest) {
    return this.http.post('/api/auth/register', userData)
  }

  async isAuthorized() {
    if (this._isAuthorized === undefined) {
      try {
        await this.http.get('/api/users/me').toPromise()
        this._isAuthorized = true
      } catch (res) {
        this._isAuthorized = false
      }
    }
    return this._isAuthorized
  }

  logout() {
    this._isAuthorized = false
    return this.http.get('/api/auth/logout')
  }
}
