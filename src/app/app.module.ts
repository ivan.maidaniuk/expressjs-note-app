import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClient, HttpClientModule} from "@angular/common/http";

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NavbarComponent} from './navbar/navbar.component';
import {RegistrationComponent} from './auth/registration/registration.component';
import {LoginComponent} from './auth/login/login.component';
import {NotesArchiveComponent} from './notes/notes-archive/notes-archive.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {UserProfileComponent} from './user-profile/user-profile.component';
import {NoteComponent} from './notes/note/note.component';
import {NoteCreatorComponent} from './notes/note-creator/note-creator.component';
import { NoteSingeComponent } from './notes/note-singe/note-singe.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    RegistrationComponent,
    LoginComponent,
    NotesArchiveComponent,
    PageNotFoundComponent,
    UserProfileComponent,
    NoteComponent,
    NoteCreatorComponent,
    NoteSingeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
