import {Component, OnInit} from '@angular/core';
import {NavigationStart, Router} from "@angular/router";
import {AuthService} from "../shared/auth.service";
import {filter} from "rxjs/operators";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  isAuthorized = false;

  constructor(
    private authService: AuthService,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.router.events
      .pipe(filter(event => event instanceof NavigationStart))
      .subscribe(async (event) => {
        this.isAuthorized = await this.authService.isAuthorized()
      });
  }

  logout() {
    this.authService.logout().subscribe()
    this.router.navigate(['/login'])
  }
}
