const {CustomError} = require('./CustomError');

/**
 * Error class for 404 issue
 * */
class UnauthorizedError extends CustomError {
  /**
   * @param {string} message
   * */
  constructor(message = 'You are not authorized') {
    super(message);
    this.status = 404;
  }
}

module.exports = {
  UnauthorizedError,
};
