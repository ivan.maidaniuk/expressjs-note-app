const {CustomError} = require('./CustomError');

/**
 * Error class for 404 issue
 * */
class IncorrectInputError extends CustomError {
  /**
   * @param {string} message
   * */
  constructor(message = 'Incorrect input data') {
    super(message);
    this.status = 400;
  }
}

module.exports = {
  IncorrectInputError,
};
