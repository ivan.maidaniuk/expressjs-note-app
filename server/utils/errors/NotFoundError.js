const {CustomError} = require('./CustomError');

/**
 * Error class for 404 issue
 * */
class NotFoundError extends CustomError {
  /**
   * @param {string} message
   * */
  constructor(message = 'Page not found') {
    super(message);
    this.status = 404;
  }
}

module.exports = {
  NotFoundError,
};
