const {} = require('../../shared/validation.schema');
const {
  loginObjSchema,
  noteContentObjSchema,
  passwordChangedSchema,
} = require('../../shared/validation.schema');
const {asyncErrorHandle} = require('../utils/api.util');

/**
 * Validate registration data
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 * */
async function loginValidation(req, res, next) {
  await loginObjSchema.validateAsync(req.body);
  next();
}

/**
 * Validate user password change
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 * */
async function changePasswordValidation(req, res, next) {
  await passwordChangedSchema.validateAsync(req.body);
  next();
}

/**
 * Note content validation
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 * */
async function noteContentValidation(req, res, next) {
  await noteContentObjSchema.validateAsync(req.body);
  next();
}


module.exports = {
  noteContentValidation: asyncErrorHandle(noteContentValidation),
  registrationValidation: asyncErrorHandle(loginValidation),
  loginValidation: asyncErrorHandle(loginValidation),
  changePasswordValidation: asyncErrorHandle(changePasswordValidation),
};
