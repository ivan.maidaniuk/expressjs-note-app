const jwt = require('jsonwebtoken');
const expressJwt = require('express-jwt');
const {asyncErrorHandle} = require('../utils/api.util');
const {UnauthorizedError} = require('../utils/errors/UnauthorizedError');
const {IncorrectInputError} = require('../utils/errors/IncorrectInputError');
const {User} = require('../models/user.model');

/**
 * Authentication middleware
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 */
async function authMiddleware(req, res, next) {
  expressJwt({
    secret: process.env.TOKEN_SECRET,
    algorithms: ['HS256'],
    getToken: (req) => req.cookies.token,
  });

  const token = req.cookies.token;
  if (!token) {
    throw new IncorrectInputError(`Json web token wasn't applied to cookie`);
  }
  const tokenPayload = await jwt.verify(token, process.env.TOKEN_SECRET);
  const userFromToken = await User.findOne({username: tokenPayload.username});
  if (!userFromToken) {
    throw new UnauthorizedError();
  }
  req.user = {
    _id: tokenPayload._id,
    username: tokenPayload.username,
  };
  next();
}

module.exports = {
  authMiddleware: asyncErrorHandle(authMiddleware),
};
