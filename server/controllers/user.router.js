const express = require('express');
const {changeUserPassword} = require('../services/user.service');
const {Note} = require('../models/note.model');
const {asyncErrorHandle} = require('../utils/api.util');
const router = new express.Router();
const {
  changePasswordValidation: changePassValidation,
} = require('../middlewares/validation.middleware');
const {User} = require('../models/user.model');

router.get('/me', asyncErrorHandle(async (req, res) => {
  const user = await User.findOne({_id: req.user._id});
  const response = {
    _id: user._id,
    username: user.username,
    createdAt: user.createdAt,
  };
  res.json({user: response});
}));

router.patch('/me', changePassValidation, asyncErrorHandle(async (req, res) => {
  const userId = {_id: req.user._id};
  const {oldPassword, newPassword} = req.body;
  await changeUserPassword({userId, oldPassword, newPassword});
  res.json({message: 'Success'});
}));

router.delete('/me', asyncErrorHandle(async (req, res) => {
  const userId = req.user._id;
  await User.findByIdAndDelete(userId);
  await Note.remove({userId});
  res.json({message: 'Success'});
}));

module.exports = {
  userRouter: router,
};
