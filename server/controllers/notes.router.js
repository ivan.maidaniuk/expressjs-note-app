const express = require('express');
const {noteContentValidation} = require('../middlewares/validation.middleware');
const router = new express.Router();

const {Note} = require('../models/note.model');
const {
  addNote,
  getNotes,
  getNoteById,
  editNoteById,
  deleteNoteById,
  toggleNoteCompletionById,
} = require('../services/note.service');
const {asyncErrorHandle} = require('../utils/api.util');


router.post('/', noteContentValidation, asyncErrorHandle(async (req, res) => {
  const userId = req.user._id;
  const {text} = req.body;
  await addNote({text, userId});
  res.json({message: 'Success'});
}));

router.get('/', asyncErrorHandle(async (req, res) => {
  const {offset = '0', limit = '0', excludeNoteId} = req.query;

  const notes = await getNotes({
    offset: +offset,
    limit: +limit,
    excludeNoteId,
    userId: req.user._id,
  });

  res.json({
    offset: +offset,
    limit: +limit,
    notes,
    count: await Note.countDocuments({userId: req.user._id}),
  });
}));

router.get('/:id', asyncErrorHandle(async (req, res) => {
  const userId = req.user._id;
  const {id} = req.params;
  const note = await getNoteById({id, userId});
  res.json({note});
}));

router.put('/:id', noteContentValidation, asyncErrorHandle(async (req, res) => {
  const userId = req.user._id;
  const {id} = req.params;
  const {text: newText} = req.body;
  await editNoteById({newText, id, userId});
  res.json({message: 'Success'});
}));

router.patch('/:id', asyncErrorHandle(async (req, res) => {
  const userId = req.user._id;
  const {id} = req.params;
  await toggleNoteCompletionById({id, userId});
  res.json({message: 'Success'});
}));

router.delete('/:id', asyncErrorHandle(async (req, res) => {
  const userId = req.user._id;
  const {id} = req.params;
  await deleteNoteById({id, userId});
  res.json({message: 'Success'});
}));


module.exports = {
  notesRouter: router,
};
