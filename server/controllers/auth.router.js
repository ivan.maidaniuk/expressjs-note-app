const express = require('express');
const {asyncErrorHandle} = require('../utils/api.util');
const {registerUser, getUserLoginToken} = require('../services/auth.service');
const {
  loginValidation,
  registrationValidation,
} = require('../middlewares/validation.middleware');
const router = new express.Router();


router.post('/register', loginValidation, asyncErrorHandle(async (req, res) => {
  const {username, password} = req.body;
  await registerUser(username, password);
  res.json({message: 'Success'});
}));

router.post(
    '/login',
    registrationValidation,
    asyncErrorHandle(async (req, res) => {
      const {username, password} = req.body;
      const token = await getUserLoginToken(username, password);
      res.cookie('token', token, {httpOnly: true});
      res.json({message: 'Success', jwt_token: token});
    }),
);

router.get('/logout', asyncErrorHandle(async (req, res) => {
  res.clearCookie('token');
  res.json({message: 'Success'});
}));

module.exports = {
  authRouter: router,
};
