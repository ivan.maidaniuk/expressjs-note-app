const {NotFoundError} = require('../utils/errors/NotFoundError');
const {Note} = require('../models/note.model');

/**
 * Add Note for User
 * @param {Object} obj
 * @param {Number} obj.text
 * @param {Number} obj.userId
 * */
async function addNote({text, userId}) {
  const note = new Note({userId, text});
  await note.save();
}

/**
 * Get user's notes
 * @param {Object} obj
 * @param {Number} obj.offset
 * @param {Number} obj.limit
 * @param {String} obj.userId
 * */
async function getNotes({offset, limit, userId, excludeNoteId}) {
  return await Note.find({userId, _id: {$ne: excludeNoteId}})
      .skip(offset)
      .limit(limit);
}

/**
 * Get user's note by id
 * @param {Object} obj
 * @param {String} obj.id
 * @param {String} obj.userId
 * */
async function getNoteById({id, userId}) {
  const note = await Note.findOne({_id: id, userId});
  if (!note) {
    throw new NotFoundError('Note with such id wasn\'t found');
  }
  return note;
}

/**
 * Edit user's note by id
 * @param {Object} obj
 * @param {String} obj.id
 * @param {String} obj.userId
 * @param {String} obj.newText
 * */
async function editNoteById({newText, id, userId}) {
  const note = await getNoteById({id, userId});
  note.text = newText;
  await note.save();
}

/**
 * Check/uncheck user's note by id
 * @param {Object} obj
 * @param {String} obj.id
 * @param {String} obj.userId
 * */
async function toggleNoteCompletionById({id, userId}) {
  const note = await getNoteById({id, userId});
  note.completed = !note.completed;
  await note.save();
}

/**
 * Delete user's note by id
 * @param {Object} obj
 * @param {String} obj.id
 * @param {String} obj.userId
 * */
async function deleteNoteById({id, userId}) {
  const note = await getNoteById({id, userId});
  await note.remove();
}

module.exports = {
  addNote,
  getNotes,
  getNoteById,
  editNoteById,
  deleteNoteById,
  toggleNoteCompletionById,
};
