const bcrypt = require('bcrypt');
const {IncorrectInputError} = require('../utils/errors/IncorrectInputError');
const {User} = require('../models/user.model');


/**
 * Change user's password
 * @param {Object} obj
 * @param {String} obj.userId
 * @param {String} obj.oldPassword
 * @param {String} obj.newPassword
 * */
async function changeUserPassword({userId, oldPassword, newPassword}) {
  const user = await User.findOne({_id: userId});

  const doesOldPassMatchEnteredOne =
      await bcrypt.compare(oldPassword, user.password);
  if (!doesOldPassMatchEnteredOne) {
    throw new IncorrectInputError(`Entered password doesn't match current one`);
  }

  user.password = await bcrypt.hash(newPassword, 10);
  await user.save();
}

module.exports = {
  changeUserPassword,
};
