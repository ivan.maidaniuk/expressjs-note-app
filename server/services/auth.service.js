const bcrypt = require('bcrypt');
const {User} = require('../models/user.model');
const jwt = require('jsonwebtoken');
const {IncorrectInputError} = require('../utils/errors/IncorrectInputError');

/**
 * Login user
 * @function
 * @param {String} username
 * @param {String} password
 * */
async function getUserLoginToken(username, password) {
  const user = await User.findOne({username});
  if (!user || !(await bcrypt.compare(password, user.password))) {
    throw new IncorrectInputError('Username or password is incorrect');
  }
  const payload = {
    _id: user._id,
    username,
  };
  const token = jwt.sign(payload, process.env.TOKEN_SECRET);
  return token;
}

/**
 * Register user
 * @function
 * @param {String} username
 * @param {String} password
 * */
async function registerUser(username, password) {
  try {
    const newUser = new User({
      username,
      password: await bcrypt.hash(password, 10),
    });
    await newUser.save();
  } catch (error) {
    if (error?.keyPattern?.username) {
      throw new IncorrectInputError('Such username is already registered');
    }
    throw error;
  }
}


module.exports = {
  getUserLoginToken,
  registerUser,
};

